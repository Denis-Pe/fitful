(ns fitful.core
    (:require [hyperfiddle.rcf :refer [tests ! %]]
              [com.rpl.specter :refer :all]
              [fitful.let-mutation :refer [let-mut]]
              [fitful.specter-additions :refer :all])
    (:import (jdk.internal.vm Continuation ContinuationScope)
      (java.util HashMap)))
;TODO remove jdk internal crap when loom is baked into jdk
; enable for testing
;(hyperfiddle.rcf/enable!)

(defprotocol CoroutineMethods
  (id [this])
  ;;todo this will have to be internal from now on? run needs to be the multimethod call
  (internal-run [this])
  (completed? [this])
  (co-yield [this] [this val])
  (current-state [this])
  (init [this scope continuation])
  (get-set-state-fn [this]))

; a coroutine is composed of a continuation, and a scope
; for more information on continuations search for project loom




(deftype Coroutine [id
                    ^:unsynchronized-mutable ^ContinuationScope scope
                    ^:unsynchronized-mutable ^Continuation continuation
                    ^:unsynchronized-mutable completed?
                    ^:unsynchronized-mutable yield-state
                    set-state-fn]
  CoroutineMethods
  (id [_] id)
  (internal-run [this] (if completed?
                         this
                         (do (.run continuation)
                             (if (.isDone continuation)
                                 (do (set! completed? true)
                                     (set! yield-state nil)))
                             yield-state)))
  (completed? [_] completed?)
  (co-yield [_] (do
                  (set! yield-state nil)
                  (Continuation/yield scope)))
  (co-yield [_ val] (do
                      (set! yield-state val)
                      (Continuation/yield scope)))
  (current-state [_] yield-state)
  (init [this s c]
    (set! scope s)
    (set! continuation c)
    this)
  (get-set-state-fn [this]
                    set-state-fn))

(defn run
      [^Coroutine this & args]
      (apply (get-set-state-fn this) args)
      (internal-run this)
      (current-state this))

;; race routines are coroutines that are composed of other coroutines
;; each time the run method is called on a RaceRoutine it runs the
;; coroutines contained in it in a linear fashion until the one of the coroutines finishes,
;; when one of the coroutines finishes, the RaceRoutine finishes

(deftype RaceCoroutine [^:unsynchronized-mutable finished?
                        coroutines]
  CoroutineMethods
  ;(run [this]
  ;  (if finished?
  ;    this
  ;    (loop [[c & cos] coroutines]
  ;      (if c
  ;        (if (and (run c) (completed? c))
  ;          (do (set! finished? true) this)
  ;          (recur cos))
  ;        coroutines))))
  (current-state [_]
    (mapv current-state coroutines))
  (completed? [_] finished?))

;; sync routines are coroutines that are composed of other coroutines
;; each time the run method is called on a SyncRoutine it runs all the
;; coroutines contained in it, it will only finish when every coroutine is finished
(deftype SyncCoroutine [^:unsynchronized-mutable finished?
                        coroutines]
  CoroutineMethods
  ;(run [this]
  ;  (if finished?
  ;    this
  ;    (do (set! finished? true)
  ;        (doseq [co coroutines]
  ;          (run co)
  ;          (if (not (completed? co))
  ;            (set! finished? false)))
  ;        this)))
  (current-state [_]
    (mapv current-state coroutines))
  (completed? [_] finished?))

(defprotocol StateMethods
  (s [this val])
  (g [this]))
; state is a type that allows us to mutate whatever we want, sometimes useful with coroutines
(deftype State [^:unsynchronized-mutable state]
  StateMethods
  (s [_ val] (set! state val))
  (g [_] state))

(defn state
  ([]
   (State. nil))
  ([start]
   (State. start)))


(defn scope
  {:doc "simple wrapper to create continuation scopes"}
  ([]
   (ContinuationScope. ""))
  ([name]
   (ContinuationScope. name)))

(defmacro wait
  {:doc "waits for a single invocation or a duration
         WARNING: meant to be used within a coroutine only"}
  ([]
   `(co-yield ~'this))
  ([time-in-seconds]
   `(let [now# (System/nanoTime)
          later# (+ now# ~(* (if (ratio? time-in-seconds)
                               (float time-in-seconds)
                               time-in-seconds)
                             1000000000))]
      (while (< (System/nanoTime) later#)
        (co-yield ~'this)))))

(defmacro wait-return
  {:doc "waits for a single invocation or a duration and returns a value
         WARNING: meant to be used within a coroutine only"}
  ([val]
   (list 'co-yield 'this val))
  ([time-in-seconds val]
   `(let [now# (System/nanoTime)
          later# (+ now# ~(* (if (ratio? time-in-seconds)
                               (float time-in-seconds)
                               time-in-seconds)
                             1000000000))]
      (while (< (System/nanoTime) later#)
        ~(list 'co-yield 'this val)))))


(defmacro race
  {:doc "takes a vector of coroutines and produces a RaceCoroutine runs that race coroutine
        until the race routine is complete, returns the RaceCoroutine when finished
        WARNING: meant to be used within a coroutine only"}
  [coroutines]
  `(let [race-coroutine# (RaceCoroutine. false ~coroutines)]
     (while (and (run race-coroutine#) (not (completed? race-coroutine#)))
       (co-yield ~(identity 'this) (current-state race-coroutine#)))
     race-coroutine#))

(defmacro sync
  {:doc "takes a vector of coroutines and produces a SyncCoroutine runs that sync coroutine
        until the sync coroutine is complete, returns the SyncCoroutine when finished
        WARNING: meant to be used within a coroutine only"}
  [coroutines]
  `(let [sync-coroutine# (SyncCoroutine. false  ~coroutines)]
     (while (and (run sync-coroutine#) (not (completed? sync-coroutine#)))
       (co-yield ~(identity 'this) (current-state sync-coroutine#)))
     sync-coroutine#))

(defn sync-coroutine
  {:doc "produces a SyncCoroutine"}
  [coroutines]
  (SyncCoroutine. false coroutines))

(defn race-coroutine
  {:doc "produces a RaceCoroutine"}
  [coroutines]
  (RaceCoroutine. false coroutines))

(defn generate-function-arg-code
      [id scope original-coroutine-sig]
      (let [[_ args & body] original-coroutine-sig
            syms (select (walker symbol?) args)
            gens (transform (walker #(and (symbol? %) (not= % '&)))
                            gensym
                            args)
            gens-flat (select (walker symbol?) gens)]
           `(let-mut ~(into [] (concat (interpose nil args) [nil]))
                     (let [scope# ~scope
                           ~'this (Coroutine. ~id nil nil false nil (fn ~gens ~@(->> (interleave syms gens-flat)
                                                                                     (partition 2)
                                                                                     (map (fn [x] (conj x 'set!))))))
                           co# (Continuation. scope# (fn [] ~@body))]
                          (init ~'this scope# co#)))))




(defmacro coroutine
  {:doc "creates a coroutine, with or without an id
         without id ex: (coroutine (scope \"hello\") (fn []))
         with id ex: (coroutine 'id-can-be-anything (scope \"hello\") (fn []))"}
  ([scope body]
   (generate-function-arg-code nil scope body))
  ([id scope body]
   (generate-function-arg-code id scope body)))


(let [scope (scope "main")
      c1    (coroutine scope (fn []))
      c2    (coroutine scope (fn [] (wait)))
      c3    (coroutine scope (fn [] (wait-return 1)))
      c4    (coroutine scope (fn [] (wait-return 1) (wait-return 100)))]
  (tests "testing-coroutines"
         true  :=  (completed? (do (run c1) c1))
         false :=  (completed? (do (run c2) c2))
         true  :=  (completed? (do (run c2) c2))
         1     :=  (run c3)
         1     :=  (run c4)
         100   :=  (run c4)
         false :=  (completed? c4)
         true  :=  (completed? (do (run c4) c4))
         ;the last returned value is the state, maybe need another name
         nil  :=   (current-state c4)))

(let [scope (scope "main")
      c1    (coroutine scope
                       (fn []
                           (let [c1 (coroutine scope
                                               (fn [] (let [x (state 0)]
                                                        (while true
                                                          (wait-return (->> (g x)
                                                                            inc
                                                                            (s x)))))))
                                 c2 (coroutine scope
                                               (fn []
                                                 (wait-return "a")
                                                 (wait-return [1 2 1])))]
                             (race [c1 c2]))))]
  (tests "testing-anaphoric-race"
         [1 "a"] := (current-state (run c1))
         [2 [1 2 1]] := (current-state (run c1))
         ;race completes as soon as one of the routines are done
         [2 [1 2 1]] := (current-state (run c1))))


(let [scope (scope "main")
      c1    (coroutine scope (fn [] (let [c1 (coroutine scope
                                                        (fn [] (let [x (state 0)]
                                                                 (while (< (g x) 5)
                                                                   (wait-return (->> (g x)
                                                                                     inc
                                                                                     (s x)))))))
                                          c2 (coroutine scope
                                                        (fn []
                                                          (wait-return "a")
                                                          (wait-return [1 2 1])))]
                                      (sync [c1 c2]))))]
  (tests "testing-anaphoric-sync"
         [1 "a"] := (run c1)
         [2 [1 2 1]] :=  (run c1)
         [3 [1 2 1]] :=  (run c1)
         [4 [1 2 1]] :=  (run c1)
         [5 [1 2 1]] :=  (run c1)
         false        := (completed? c1)
         ; sync stops only when all the coroutines have completed
         [5 [1 2 1]] := (run c1)
         true        := (completed? c1)))

(comment (def keep-going (atom false))

         (def kek (let [scope (scope "main")
                        co (coroutine scope (fn [] (while @keep-going
                                                          (println "hello")
                                                          (wait 0.5)
                                                          (println "goodbye")
                                                          (wait-return 0.5 100))))]
                       (Thread/startVirtualThread (fn [] (while @keep-going (println (run co)))))))






         (def x (coroutine (scope "main") (fn [x] (while true (wait-return x)))))

         (let [s (scope "main")
               c1 (coroutine s (fn [x] (while true (wait-return x))))
               c2 (coroutine s (fn [x] (while (< x 3) (wait-return (str "this is: " x)))))]
              (map (fn [x] (run x 2)) [c1
                                       c2]))

         (let [depth (coroutine (scope "main")
                                (fn [option]
                                    (let-mut [s {:depth 0}]
                                             (while true
                                                    (cond (= :increase option)
                                                          (set! s (update s :depth inc))
                                                          (= :decrease option)
                                                          (set! s (update s :depth dec)))
                                                    (println s)
                                                    (wait-return s)))))]
              (select (recursive-path [] p
                                      (if-path sequential?
                                               [(exe (run depth :increase)) (continue-then-stay [ALL p])
                                                (if-path sequential?
                                                         [(exe (run depth :decrease) (println structure)) STOP]
                                                         STAY)]

                                               STAY))
                      [1 2 3 [1 2]])))
;java coroutine generator







