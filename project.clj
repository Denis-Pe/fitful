(defproject fitful "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :plugins [[reifyhealth/lein-git-down "0.4.1"]]
  :repositories [["public-github" {:url "git://github.com"}]
                 ["private-github" {:url "git://github.com" :protocol :ssh}]]
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [com.rpl/specter "1.1.4"]
                 [hyperfiddle/rcf "af197a85749898e76262d488a245d23748c38edb"]
                 [io.github.nextjournal/clerk "0.7.418"]]
  :java-source-paths ["src"]
  :repl-options {:init-ns fitful.core})
