(ns fitful.let-mutation
    (:require [com.rpl.specter :refer :all]
              [fitful.specter-additions :refer :all])
    (:import (let_state LetState)))

;;TODO come back and clean this up, it is really messy

(defn binding-form?
      [form]
      (if (and (sequential? form) (some '#{let for bind-me let-mut fn defn loop} form))
        true
        false))

(defn extract-bindings
      [binding-form]
      (into #{} (flatten (map (fn [[b]]
                                  (select (recursive-path [] p
                                                          (if-path #(or (sequential? %) (map? %))
                                                                   [ALL p]
                                                                   #(and (symbol? %) (not= '& %))))
                                          b))
                              (partition 2 binding-form)))))

(defn bind-state-let-mut
      [form]
      (let [[_ bindings & body] form
            state-bindings (extract-bindings bindings)]
           `(~'let ~(into [] (mapcat identity
                                     (transform [ALL #(some state-bindings [(first %)]) LAST]
                                                (fn [x] `(~'new ~'let_state.LetState ~x))
                                                (partition 2 (destructure bindings)))))
              ~@body)))

(defn determine-expansion!
      [a s]
      (if (= (first s) 'let-mut)
        (swap! a assoc :add true)
        (swap! a assoc :add false)))


(def DETERMINE-BIND (recursive-path [add?] p
                                    (if-path #(or (sequential? %) (map? %))
                                             [ALL p]
                                             (if-path symbol?
                                                      (transformed STAY #(if (not= % '&)
                                                                             (if (:add @add?)
                                                                                 (with-meta % {:add true})
                                                                                 (with-meta % {:add false}))))))))

(defn unroll-let-mut
      [code]
      (let [walker (recursive-path [expand-resolve] p
                                   [(cond-path binding-form?
                                               [(exe (determine-expansion! expand-resolve structure))
                                                (skip 1)
                                                (continue-then-stay [FIRST INDEXED-VALS
                                                                     (fn [[idx val]] (odd? idx))
                                                                     LAST p])
                                                (if-path sequential?
                                                         (continue-then-stay [FIRST INDEXED-VALS
                                                                              (fn [[idx val]] (even? idx))
                                                                              LAST
                                                                              (DETERMINE-BIND expand-resolve)
                                                                              p])
                                                         STAY)
                                                (if-path sequential?
                                                         [(skip 1) p]
                                                         STAY)]
                                               symbol? STAY
                                               sequential? (cond-path [FIRST (pred= 'set!)]
                                                                      [(transformed FIRST (fn [_] '.setState))
                                                                       LAST
                                                                       (if-path sequential?
                                                                                [ALL p]
                                                                                STAY)]
                                                                      [FIRST (pred= 'quote)]
                                                                      STAY
                                                                      identity [ALL p])
                                               :else STOP)])
            get-state-transform (transform (subselect (walker (atom {})))
                                           (fn [syms]
                                               (:ret (reduce (fn [acc n]
                                                                 (if-let [bind (meta n)]
                                                                         (update (if (:add bind)
                                                                                     (update acc :bindings conj n)
                                                                                     (update acc :bindings disj n))
                                                                                 :ret
                                                                                 (fn [x] (concat x [n])))
                                                                         (update acc :ret (fn [x]
                                                                                              (if (contains? (:bindings acc) n)
                                                                                                  (concat x `((~'.getState ~n)))
                                                                                                  (concat x [n]))))))
                                                             {:bindings #{} :ret '()}
                                                             syms)))
                                           code)]
           (clojure.walk/postwalk
             (fn [form]
                 (if (and (sequential? form) (= 'let-mut (first form)))
                     (bind-state-let-mut form)
                     form))
             get-state-transform)))

(defmacro let-mut
          [& code]
          (unroll-let-mut (conj code 'let-mut)))

;(macroexpand-1 '(let-mut [x 1 z 3] (set! x 1) 'x + 1 2 3 x (fn [x] x z)))