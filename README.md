# fitful

Fitful is a library for using coroutines in Clojure.

## Usage

FitFul was created using a windows binary from project loom: 
https://download.java.net/java/early_access/loom/4/openjdk-19-loom+4-115_windows-x64_bin.zip

check the bottom of the core file for code examples

## License

Copyright © 2022 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
