package let_state;

public class LetState {
    private Object state;

    public LetState(Object state) {
        this.state = state;
    }

    public Object getState() {
        return state;
    }

    public Object setState(Object state) {
        this.state = state;
        return state;
    }
}
